import sqlite3
from sqlite3 import Error
from datetime import datetime, timedelta
from werkzeug.security import generate_password_hash
import time
 

create_table_tootaja = """ CREATE TABLE IF NOT EXISTS tootaja (
                            id INTEGER PRIMARY KEY,
                            username VARCHAR(50) NOT NULL,
                            password_hash VARCHAR(406) NOT NULL,
                            rights VARCHAR(50) NOT NULL
                          );
                      """
table_tootaja_insert = "INSERT INTO tootaja (username, password_hash, rights) VALUES (?, ?, ?)"
table_tootaja_select = "SELECT * FROM tootaja"

create_table_raamat = """ CREATE TABLE IF NOT EXISTS raamat (
                            id INTEGER PRIMARY KEY,
                            name VARCHAR(225) NOT NULL,
                            author VARCHAR(225) NOT NULL,
                            borrow_time_weeks INTEGER NOT NULL,
                            quantity INTEGER NOT NULL,
                            library_section VARCHAR(2) NOT NULL
                          );
                      """
table_raamat_insert = "INSERT INTO raamat (name, author, borrow_time_weeks, quantity, library_section) VALUES (?, ?, ?, ?, ?)"
# https://www.theguardian.com/books/2003/oct/12/features.fiction
raamat_values = [
    ("Don Quixote", "Miguel De Cervantes", 1, 4, "A2"),
    ("Pilgrim's Progress", "John Bunyan", 3, 10, "B1"),
    ("Robinson Crusoe", "Daniel Defoe", 2, 8, "B2"),
    ("Gulliver's Travels", "Jonathan Swift", 4, 9, "A4"),
    ("Tom Jones", "Henry Fielding", 3, 5, "A1"),
    ("Clarissa", "Samuel Richardson", 2, 6, "B2"),
    ("Tristram Shandy", "Laurence Sterne", 2, 7, "B3"),
    ("Dangerous Liaisons", "Pierre Choderlos De Laclos", 1, 2, "A4"),
    ("Emma", "Jane Austen", 4, 10, "A2"),
    ("Frankenstein", "Mary Shelley", 4, 5, "B1"),
    ("Nightmare Abbey", "Thomas Love Peacock", 3, 6, "A4"),
    ("The Black Sheep", "Honoré De Balzac", 2, 9, "A3"),
    ("The Charterhouse of Parma", "Stendhal", 4, 10, "A1"),
    ("The Count of Monte Cristo", "Alexandre Dumas", 1, 1, "B4"),
    ("Sybil", "Benjamin Disraeli", 1, 2, "B3"),
    ("David Copperfield", "Charles Dickens", 1, 3, "A1"),
    ("Wuthering Heights", "Emily Bronte", 3, 7, "A2"),
    ("Jane Eyre", "Charlotte Bronte", 2, 8, "B3"),
    ("Vanity Fair", "William Makepeace Thackeray", 4, 6, "B4"),
    ("The Scarlet Letter", "Nathaniel Hawthorne", 1, 4, "A4"),
    ("Moby-Dick", "Herman Melville", 2, 5, "A2"),
    ("Madame Bovary", "Gustave Flaubert", 3, 9, "B1"),
    ("The Woman in White", "Wilkie Collins", 2, 10, "A1"),
    ("Alice's Adventures In Wonderland", "Lewis Carroll", 4, 7, "B1"),
    ("Little Women", "Louisa M. Alcott", 4, 6, "B3"),
    ("The Way We Live Now", "Anthony Trollope", 4, 5, "A2"),
    ("Anna Karenina", "Leo Tolstoy", 1, 2, "B4"),
    ("Daniel Deronda", "George Eliot", 2, 5, "A1"),
    ("The Brothers Karamazov", "Fyodor Dostoevsky", 3, 8, "B4"),
    ("The Portrait of a Lady", "Henry James", 2, 7, "B2"),
    ("Huckleberry Finn", "Mark Twain", 1, 3, "B1"),
    ("The Strange Case of Dr Jekyll and Mr Hyde", "Robert Louis Stevenson", 4, 6, "A2"),
    ("Three Men in a Boat", "Jerome K. Jerome", 2, 8, "B2"),
    ("The Picture of Dorian Gray", "Oscar Wilde", 1, 4, "A4"),
    ("The Diary of a Nobody", "George Grossmith", 1, 4, "A3"),
    ("Jude the Obscure", "Thomas Hardy", 3, 6, "A1"),
    ("The Riddle of the Sands", "Erskine Childers", 2, 10, "B2"),
    ("The Call of the Wild", "Jack London", 2, 8, "B4"),
    ("Nostromo", "Joseph Conrad", 4, 7, "A4"),
    ("The Wind in the Willows", "Kenneth Grahame", 1, 3, "A3"),
    ("In Search of Lost Time", "Marcel Proust", 2, 7, "A1"),
    ("The Rainbow", "D. H. Lawrence", 3, 8, "B2"),
    ("The Good Soldier", "Ford Madox Ford", 4, 9, "B1"),
    ("The Thirty-Nine Steps", "John Buchan", 4, 10, "B3"),
    ("Ulysses", "James Joyce", 3, 5, "A2"),
    ("Mrs Dalloway", "Virginia Woolf", 2, 6, "A3"),
    ("A Passage to India", "EM Forster", 1, 1, "A1"),
    (" The Great Gatsby", "F. Scott Fitzgerald", 2, 5, "A4"),
    ("The Trial", "Franz Kafka", 4, 8, "B2"),
    ("Men Without Women", "Ernest Hemingway", 1, 2, "B1")
]
table_raamat_select = "SELECT * FROM raamat"

create_table_laenutaja = """ CREATE TABLE IF NOT EXISTS laenutaja (
                            id INTEGER PRIMARY KEY,
                            name VARCHAR(125) NOT NULL
                          );
                      """
table_laenutaja_insert = "INSERT INTO laenutaja (username, password_hash, rights) VALUES (?)"
table_laenutaja_select = "SELECT * FROM laenutaja"

create_table_laenutus = """ CREATE TABLE IF NOT EXISTS laenutus (
                            id INTEGER PRIMARY KEY,
                            laenutaja_id INTEGER NOT NULL,
                            raamat_id INTEGER NOT NULL,
                            starting_time DATETIME NOT NULL,
                            delay INTEGER NOT NULL
                          );
                      """
table_laenutus_insert = "INSERT INTO laenutus (laenutaja_id, raamat_id) VALUES (?, ?);"
table_laenutus_select = "SELECT * FROM laenutus"

def sqlconnection(database):
    connection = None
    try:
        connection = sqlite3.connect(database)
    except Error as e:
        print(e)
    return connection
 
def getMainUsersAndPasswords(shouldnt_actually_exist_file):
    user = None
    password = None
    user_dict = dict()
    with open(shouldnt_actually_exist_file, "r", encoding = "utf8") as saef:
        for line in saef:
            line_splitted = line.split(";")
            user = line_splitted[0].replace("\n", "")
            password = line_splitted[1].replace("\n", "")
            user_dict[user] = password
    return user_dict

def calculate_days_delayed(start_date, weeks):
    current_date = datetime.now()
    borrow_days = timedelta(days = weeks*7)
    start_date_splitted = start_date.split(" ")
    start_date_days = start_date_splitted[0].split("-")
    start_date_seconds = start_date_splitted[1].split(":")
    start = datetime(start_date_days[0], start_date_days[1], start_date_days[2], start_date_seconds[0], start_date_seconds[1], start_date_seconds[2])
    borrow_end_datetime = start + borrow_days
    if borrow_end_datetime < current_date:
        return (current_date - borrow_end_datetime).days
    else:
        return False

class User:
    def __init__(self, username, password, rights):
        self.username = username
        self.password = password
        self.rights = rights
    
    def getUsername(self):
        return self.username
    
    def getPassword(self):
        return self.password
    
    def setPassword(self, password):
        self.password = generate_password_hash(self.password, method="pbkdf2:sha512", salt_length=256)
    
    def getRights(self):
        return self.rights
 
if __name__ == '__main__':
    # Eeldame, et raamatukogu rakendus alles loodi ja sinna pole ühtegi kasutajat kaasatud.
    # Kuna raamatukogu tegevust haldavad administraator ja teenindaja, siis loome mõlemad kasutajad. 
    # NB! Nende kahe kasutaja nimed koos paroolidega on kasutajad_luua.txt failis kindlaks määratud, aga loomulikult võib neid seal ümber muuta.
    # Ideaalis peaks olema rakendusel juures registreerimise vorm, kuid antud rakenduse puhul pidevat registreerimist vaja pole, sest ülesande 
    # kirjelduse põhjal on aru saada, et laenutajad pole registreeritud kasutajad, vaid nende laenutamiste kohta peab raamatukogu rakendus lihtsalt järge.
    main_users_dict = getMainUsersAndPasswords("kasutajad_luua.txt")
    user_administraator = None
    user_teenindaja = None
    for user_rights in main_users_dict:
        if user_rights == "raamatukogu_administraator":
            user_administraator = User("admin", main_users_dict[user_rights], user_rights)
        if user_rights == "raamatukogu_teenindaja":
            user_teenindaja = User("teenindaja", main_users_dict[user_rights], user_rights)
    user_administraator_password = user_administraator.getPassword()
    #print(user_administraator_password)
    user_administraator.setPassword(user_administraator_password)
    #print(user_administraator.getPassword())
    user_teenindaja_password = user_teenindaja.getPassword()
    #print(user_teenindaja_password)
    user_teenindaja.setPassword(user_teenindaja_password)
    #print(user_teenindaja.getPassword())
    # Nüüd on kahe põhikasutaja objektid loodud.
    # Edasi loome andmebaasi. Sinna tekitame tabelid tootaja, raamat, laenutaja ja laenutus. Lisame kohe andmebaasi ära 50 raamatut (tabel raamat) ja põhikasutajad (tabel tootaja).
    # Tabel laenutaja iseloomustab raamatu laenutaja andmeid ning tabel laenutus kirjeldab id-põhist seost laenutaja ja laenutatud raamatu vahel.
    connection = sqlconnection("library_database.db")
    cursor = connection.cursor()
    connection.execute(create_table_raamat)
    connection.execute(create_table_tootaja)
    connection.execute(create_table_laenutaja)
    connection.execute(create_table_laenutus)
    # Lisame raamatukogus olevad raamatud ja põhikasutajad vastavatesse tabelitesse.
    connection.executemany(table_raamat_insert, raamat_values)
    connection.execute(table_tootaja_insert, (user_administraator.getUsername(), user_administraator.getPassword(), user_administraator.getRights()))
    connection.execute(table_tootaja_insert, (user_teenindaja.getUsername(), user_teenindaja.getPassword(), user_teenindaja.getRights()))
    connection.commit()