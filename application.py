from flask import Flask, session, render_template, request, flash
from werkzeug.security import check_password_hash
import os, logging
import sqlite3
from sqlite3 import Error
from setup import sqlconnection

logging.basicConfig(filename = "library_system.log", format = "%(asctime)s - %(process)d - %(levelname)s - %(message)s", datefmt = "%Y-%m-%d %H:%M:%S", level = logging.INFO)
logger = logging.getLogger() 

app = Flask(__name__)

@app.route("/")
def main_page():
    connection = sqlconnection("library_database.db")
    cursor = connection.cursor()
    cursor.execute("SELECT * FROM raamat")
    raamat_data = cursor.fetchall()
    books_list = [] 
    authors_list = []  
    borrow_time_list = []  
    sections_list = []
    quantity_list = []
    for book in raamat_data:
        name = book[1].replace('"', "").strip()
        author = book[2].replace('"', "").strip()
        borrow_time = book[3]
        quantity = book[4]
        section = book[5].replace('"', "").strip()
        books_list.append(name)
        authors_list.append(author)
        borrow_time_list.append(borrow_time)
        sections_list.append(section)
        quantity_list.append(quantity)
    return render_template("library.html", books = books_list, authors = authors_list, borrow_periods = borrow_time_list, sections = sections_list, quantities = quantity_list)

@app.route("/app")
def application():
    if session.get("logged_in"):
        return render_template("mainpage.html")
    else:
        return render_template("loginform.html")

@app.route("/login", methods = ["POST"])
def login():
    user = request.form["username"]
    connection = sqlconnection("library_database.db")
    cursor = connection.cursor()
    cursor.execute("SELECT password_hash FROM tootaja WHERE username = \"{}\"".format(user))
    phash_fetch = cursor.fetchall()
    phash = None
    if len(phash_fetch) > 0: 
        phash = phash_fetch[0][0]
    if phash is not None and check_password_hash(phash, request.form["password"]):
        session["logged_in"] = True
        logger.info("User {} has logged in.".format(user)) 
    else:
        flash("Incorrect password!")
        wrong_user = request.form["username"]
        wrong_password = user = request.form["password"]
        logger.warning("Incorrect username ({}) or password ({}) used.".format(wrong_user, wrong_password))
    return application()

@app.route("/logout")
def logout():
    session["logged_in"] = False
    logger.info("Logged out from system.") 
    return main_page()

if __name__ == "__main__":
    app.secret_key = os.urandom(24)
    app.run(debug = True, host = "0.0.0.0", port = 5000)