# RaamatukoguProovitoo

Nõuded:

*  Python 3.7
*  Lisaks Flask moodulile kasutusel veel sqlite3, werkzeug.security ja timedelta moodulid, mis tuleks eelnevalt paigaldada (pip install).

Kirjeldus:

*  Käivitada esmalt setup.py, mis loob library_database.db faili ehk andmebaasi osa. Rohkem seda mõnes hilisemas faasis käivitada pole vaja.
*  kasutajad_luua.txt failis on paika pandud admin ja teenindaja kasutajad koos paroolidega. See pole hea tava need faili kirja panna, aga reaalses stsenaariumis faili ei peaks eksisteerima. NB! Failis olevad kasutajanimed (esimeses veerus, enne semikoolonit) pole kasutusel, vaid need muudetakse koodis ära vastavalt kui "admin" ja "teenindaja". Samuti võib paroolid kohe alguses antud failis ümber muuta.
*  application.py käivitab rakenduse. Testida localhost'ina, port 5000 (ehk http://localhost:5000/)
*  Senise lahenduse juurde on tehtud kolm algelist html-faili (template kaustas). login.html (rakenduses /app) iseloomustab sisselogimislehte, kuhu saab seega sisestada kasutaja ja parooli ning nupu "Log in" vajutusel suunatakse ümber mainpage.html lehele. Seal on praeguse seisuga olemas vaid link väljalogimiseks. library.html (seatud vaikimisi rakenduse leheks) iseloomustab raamatukogu seisu.
*  Rakenduse kasutamist logitakse ja tekkiva logifaili nimeks on library_system.log

Märkus:

*  Kõik seni koodis kirjapandu ei ole ülesande eesmärki silmas pidades lõplik.
